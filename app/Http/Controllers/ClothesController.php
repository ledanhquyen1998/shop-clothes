<?php

namespace App\Http\Controllers;

use App\Http\Service\ClothesServiceInterface;
use Illuminate\Http\Request;

class ClothesController extends Controller
{
    protected $ClothesService;

    public function __construct(ClothesServiceInterface $ClothesService)
    {
        $this->ClothesService = $ClothesService;
    }

    protected function getAll()
    {
        $data['clothes'] = $this->ClothesService->getAll();
        return view('clothes.index', $data);
    }
}
