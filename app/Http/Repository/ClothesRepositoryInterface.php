<?php


namespace App\Http\Repository;


interface ClothesRepositoryInterface
{
    public function getAll();

    public function add($data);
}
