<?php


namespace App\Http\Repository\eloquent;


use App\Clothes;
use App\Http\Repository\ClothesRepositoryInterface;

class ClothesRepository extends BaseRepository implements ClothesRepositoryInterface
{
    protected $clothes;

    public function __construct(Clothes $clothes)
    {
        $this->clothes = $clothes;
    }

    public function getAll()
    {
        return $this->clothes->all();
    }

    public function add($data)
    {
        return $data->save();
    }
}
