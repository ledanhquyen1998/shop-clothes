<?php


namespace App\Http\Service\Implement;


use App\Clothes;
use App\Http\Repository\ClothesRepositoryInterface;
use App\Http\Service\ClothesServiceInterface;

class ClothesService extends BaseService implements ClothesServiceInterface
{
    protected $clothesRepository;
    protected $clothes;

    public function __construct(ClothesRepositoryInterface $clothesRepository,
                                Clothes $clothes)
    {
        $this->clothesRepository = $clothesRepository;
        $this->clothes = $clothes;
    }

    public function getAll()
    {
        return $this->clothesRepository->getAll();
    }

    public function add($request)
    {
        $data = $this->clothes;
        $data->title = $request->title;
        $data->price = $request->price;
        $data->size_id = $request->size_id;
        $data->type_id = $request->type_id;
        return $this->clothesRepository->add($data);
    }
}
