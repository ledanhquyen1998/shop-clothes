<?php


namespace App\Http\Service;


interface ClothesServiceInterface
{
    public function getAll();
    public function add($request);
}
