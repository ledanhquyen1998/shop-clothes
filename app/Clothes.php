<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clothes extends Model
{
    protected $table = 'clothes';
    public function size()
    {
        return $this->belongsTo('App\Size');
    }
}
