<?php

namespace App\Providers;

use App\Http\Repository\ClothesRepositoryInterface;
use App\Http\Repository\eloquent\ClothesRepository;
use App\Http\Service\ClothesServiceInterface;
use App\Http\Service\Implement\ClothesService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ClothesServiceInterface::class, ClothesService::class);
        $this->app->singleton(ClothesRepositoryInterface::class, ClothesRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
